import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { CommonService } from 'src/app/common.service';
import { HttpService } from 'src/app/http.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  equipmentName: any;
  contactDetails: any;
  label: any;
  datepurchase: any;
  serialNo: any;
  resequip: any;
  mf: any;
  ds: any;
  eqpName: any;
  contact: any;
  lbl: any;
  dateofPurchase: any;
  serialno: any;
  reseqp: any;
  manufacturer: any;
  dateService: any;
  isEdit = false;
  isUpdate = false;
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol', 'timeFrom', 'timeTo', 'downTimeFrom', "action"];

  dataSource: any;
  maintenanceForm = this.fb.group({
    date: new FormControl(null, Validators.required),
    descrp: new FormControl(null, Validators.required),
    mpb: new FormControl(null, Validators.required),
    dateServ: new FormControl(null, Validators.required),
    vpb: new FormControl(null, Validators.required),
    planneddate: new FormControl(null, Validators.required),
    remarks: new FormControl(null, Validators.required),



  })
  constructor(public fb: FormBuilder, private httpService: HttpService, public datepipe: DatePipe, public commonService: CommonService) {

  }

  ngOnInit(): void {
    const formData = new FormData();
    formData.append('GetTableName', 'Maintainance_Logsheet');
    this.httpService.post("Select_Table", formData).subscribe((res: any) => {
      this.dataSource = res;
      this.equipmentName = res[0].eqpName;
      this.contactDetails = res[0].contact;
      this.lbl = res[0].label;
      this.datepurchase = res[0].dateofPurchase;
      this.serialNo = res[0].serialno;
      this.resequip = res[0].reseqp;
      this.mf = res[0].manufacturer;
      this.ds = res[0].dateService;
    })
    // this.dataSource = this.commonService.tableData;
    let user = localStorage.getItem('currentUser')
    if (user === 'carbyne123') {
      this.isEdit = true;
    }
    console.log(this.dataSource)
  }

  onSubmit() {

    this.dateofPurchase = this.datepipe.transform(this.dateofPurchase, 'yyyy-MM-dd');
    this.dateService = this.datepipe.transform(this.dateService, 'yyyy-MM-dd');
    let details = [];
    details.push(this.eqpName, this.contact, this.label, this.dateofPurchase, this.serialno, this.reseqp, this.manufacturer, this.dateService,);
    let columns: any = [];
    columns = Object.values(this.maintenanceForm.value);
    columns = [...details, ...columns]
    console.log(columns)
    const formData = new FormData();
    formData.append('ColumnValues', JSON.stringify(columns));
    formData.append('GetTableName', 'Maintainance_Logsheet');

    this.httpService.post("Insert_Table", formData).subscribe((res: any) => {
      if (res) {
        this.dataSource = res;
        this.commonService.tableData = res;
        console.log(this.dataSource, "llllllllll")

        this.equipmentName = this.eqpName;
        this.contactDetails = this.contact;
        this.lbl = this.label;
        this.datepurchase = this.dateofPurchase;
        this.serialNo = this.serialno;
        this.resequip = this.reseqp;
        this.mf = this.manufacturer;
        this.ds = this.dateService;
        // console.log("data", this.dataSource, this.machine);
        this.maintenanceForm.reset();
        Object.keys(this.maintenanceForm.controls).forEach(key => {
          this.maintenanceForm.controls[key].setErrors(null);
        });
      }
    })
  }

  onEdit(ele: any) {
    this.isUpdate = true
    this.maintenanceForm.patchValue(ele);
    this.eqpName = ele.eqpName;
    this.contact = ele.contact;
    this.label = ele.label;
    this.dateofPurchase = ele.dateofPurchase;
    this.serialno = ele.serialno;
    this.reseqp = ele.reseqp;
    this.manufacturer = ele.manufacturer;
    this.dateService = ele.dateService;
    const element: any = document.querySelector("#formData");
    element.scrollIntoView();

  }
  onUpadte() {
    this.dateofPurchase = this.datepipe.transform(this.dateofPurchase, 'yyyy-MM-dd');
    this.dateService = this.datepipe.transform(this.dateService, 'yyyy-MM-dd');
    let details = [];
    details.push(this.eqpName, this.contact, this.label, this.dateofPurchase, this.serialno, this.reseqp, this.manufacturer, this.dateService,);
    let columns: any = [];
    columns = Object.values(this.maintenanceForm.value);
    columns = [...details, ...columns]
    console.log(columns)
    const formData = new FormData();
    formData.append('updatedValues', JSON.stringify(columns));
    formData.append('GetTableName', 'Maintainance_Logsheet');
    formData.append('updatedID', this.maintenanceForm.value.date)
    this.httpService.post('Update_Table', formData).subscribe((res) => {
      this.dataSource = res;
      this.isUpdate = false;
    })
  }

}
