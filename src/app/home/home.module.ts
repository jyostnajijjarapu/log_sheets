import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from '../material/material.module';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

import { ReportsComponent } from './reports/reports.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ProcessFlowComponent } from './process-flow/process-flow.component';
// import { ProcessModalComponent } from './process-flow/process-modal/process-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    HomeComponent, NavbarComponent, ProcessFlowComponent, ReportsComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MaterialModule,


    FormsModule, ReactiveFormsModule

  ]
})
export class HomeModule { }
