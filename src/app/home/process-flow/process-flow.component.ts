import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { HttpService } from 'src/app/http.service';
import { DatePipe } from '@angular/common'
import { CommonService } from 'src/app/common.service';


// export interface PeriodicElement {
//   name: string;
//   position: number;
//   weight: number;
//   symbol: string;
//   timeFrom: string;
//   downTimeFrom: string;
//   reasonCode: number;
//   reason: string;
//   sapdoc: string;
//   doc: string;
//   timeTo: string,
//   downTimeTo: string;

// }

// const ELEMENT_DATA: PeriodicElement[] = [
//   { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H', timeFrom: 'hyd', timeTo: 'hyd', downTimeFrom: "jyu", downTimeTo: "jyu", reasonCode: 1, reason: 'Hydrogen', sapdoc: 'huge', doc: 'june' },
//   { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He', timeFrom: 'hyd', timeTo: 'hyd', downTimeFrom: "jyu", downTimeTo: "jyu", reasonCode: 1, reason: 'Hydrogen', sapdoc: 'huge', doc: 'june' },
//   { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li', timeFrom: 'hyd', timeTo: 'hyd', downTimeFrom: "jyu", downTimeTo: "jyu", reasonCode: 1, reason: 'Hydrogen', sapdoc: 'huge', doc: 'june' },
//   { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be', timeFrom: 'hyd', timeTo: 'hyd', downTimeFrom: "jyu", downTimeTo: "jyu", reasonCode: 1, reason: 'Hydrogen', sapdoc: 'huge', doc: 'june' },
//   { position: 5, name: 'Boron', weight: 10.811, symbol: 'B', timeFrom: 'hyd', timeTo: 'hyd', downTimeFrom: "jyu", downTimeTo: "jyu", reasonCode: 1, reason: 'Hydrogen', sapdoc: 'huge', doc: 'june' },
//   { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C', timeFrom: 'hyd', timeTo: 'hyd', downTimeFrom: "jyu", downTimeTo: "jyu", reasonCode: 1, reason: 'Hydrogen', sapdoc: 'huge', doc: 'june' },
//   { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N', timeFrom: 'hyd', timeTo: 'hyd', downTimeFrom: "jyu", downTimeTo: "jyu", reasonCode: 1, reason: 'Hydrogen', sapdoc: 'huge', doc: 'june' },
//   { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O', timeFrom: 'hyd', timeTo: 'hyd', downTimeFrom: "jyu", downTimeTo: "jyu", reasonCode: 1, reason: 'Hydrogen', sapdoc: 'huge', doc: 'june' },
//   { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F', timeFrom: 'hyd', timeTo: 'hyd', downTimeFrom: "jyu", downTimeTo: "jyu", reasonCode: 1, reason: 'Hydrogen', sapdoc: 'huge', doc: 'june' },
//   { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne', timeFrom: 'hyd', timeTo: 'hyd', downTimeFrom: "jyu", downTimeTo: "jyu", reasonCode: 1, reason: 'Hydrogen', sapdoc: 'huge', doc: 'june' },
// ];


@Component({
  selector: 'app-process-flow',
  templateUrl: './process-flow.component.html',
  styleUrls: ['./process-flow.component.scss']
})
export class ProcessFlowComponent implements OnInit {
  // data: any;
  machine: any;
  operator: any;
  dateShift: any;
  machineName: any;
  operatorName: any;
  date: any;
  shift: any;
  date1: any;
  isEdit = false;
  isUpdate = false;
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol', 'timeFrom', 'timeTo', 'downTimeFrom', 'downTimeTo', 'reasonCode', 'reason', 'sapdoc', "action"];
  downTime: string[] = ["A Want to Fabric & Material", "B Fabric Scrap", "C Fabric Jamming in Carriage", "D Blade Change", "E Fabric/BW Problem", "F Process Problem", "G Tech Follow Up",
    "H Role Change", "I Mech.B/D", "J Elect.B/D", "K Want Of lirier Shell", "L Want of Linner", "M No Schedule", "N Preventive Maintenance", "O Stop Taking", "P Under Project Work", "Q Power Off", "R Want Of Let Of Sheel", "S Want Of Man Power", "T TBM", "U Stop Production Intimation", "V Lunch"]
  dataSource: any;
  logForm = this.fb.group({
    trtNo: new FormControl(null, Validators.required),
    size: new FormControl(null, Validators.required),
    noOfRoll: new FormControl(null, Validators.required),
    outputMeters: new FormControl(null, Validators.required),
    runningTimeFrom: new FormControl(null, Validators.required),
    runningTimeTo: new FormControl(null, Validators.required),
    downTimeFrom: new FormControl(null, Validators.required),
    downTimeTo: new FormControl(null, Validators.required),
    sapDocument: new FormControl(null, Validators.required),
    reasonCode: new FormControl(null, Validators.required),
    reason: new FormControl(null, Validators.required)


  })
  constructor(public fb: FormBuilder, private httpService: HttpService, public datepipe: DatePipe, public commonService: CommonService) {

  }

  ngOnInit(): void {
    const formData = new FormData();
    formData.append('GetTableName', 'Insert_Logsheet');
    this.httpService.post("Select_Table", formData).subscribe((res: any) => {
      this.dataSource = res;
      this.machine = res[0].machineName;
      this.operator = res[0].operatorName;
      let date = new Date(res[0].date)
      this.dateShift = this.datepipe.transform(date, 'yyyy-MM-dd') + ',' + res[0].shift;
    })
    this.dataSource = this.commonService.tableData;
    let user = localStorage.getItem('currentUser')
    if (user === 'carbyne123') {
      this.isEdit = true;
    }
  }

  onSubmit() {

    this.date = this.datepipe.transform(this.date, 'yyyy-MM-dd');
    let details = [];
    details.push(this.machineName, this.operatorName, this.date, this.shift);
    let columns: any = [];
    columns = Object.values(this.logForm.value);
    columns = [...details, ...columns]
    const formData = new FormData();
    formData.append('GetTableName', 'Insert_Logsheet');
    formData.append('ColumnValues', JSON.stringify(columns));
    this.httpService.post("Insert_Table", formData).subscribe((res: any) => {
      if (res) {
        this.dataSource = res;
        this.machine = this.machineName;
        this.operator = this.operatorName;
        this.dateShift = this.date + ',' + this.shift;

        this.logForm.reset();
        Object.keys(this.logForm.controls).forEach(key => {
          this.logForm.controls[key].setErrors(null);
        });
      }
    })
  }

  onEdit(ele: any) {
    this.isUpdate = true
    this.logForm.patchValue(ele);
    this.machineName = ele.machineName;
    this.operatorName = ele.operatorName;
    this.date = new Date(ele.date);
    this.shift = ele.shift;
    const element: any = document.querySelector("#formData");
    element.scrollIntoView();
  }
  onUpadte() {
    this.date = this.datepipe.transform(this.date, 'yyyy-MM-dd');
    let details = [];
    details.push(this.machineName, this.operatorName, this.date, this.shift);
    let columns: any = [];
    columns = Object.values(this.logForm.value);
    columns = [...details, ...columns]
    const formData = new FormData();
    formData.append('GetTableName', 'Insert_Logsheet');
    formData.append('updatedValues', JSON.stringify(columns));
    formData.append('updatedID', this.logForm.value.trtNo)
    this.httpService.post('Update_Table', formData).subscribe((res) => {
      this.dataSource = res;
      this.machine = this.machineName;
      this.operator = this.operatorName;
      this.dateShift = this.date + ',' + this.shift;
      this.isUpdate = false;
    })
  }
}
