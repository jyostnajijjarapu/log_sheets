import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessdeniedComponent } from './accessdenied/accessdenied.component';
import { HomeComponent } from './home.component';
import { ProcessFlowComponent } from './process-flow/process-flow.component';
import { ReportsComponent } from './reports/reports.component';

const routes: Routes = [
  {
    path: '', component: HomeComponent,
    children: [
      {
        path: 'reports', component: ReportsComponent

      },
      { path: 'process', component: ProcessFlowComponent },
      { path: 'accessDenied', component: AccessdeniedComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
