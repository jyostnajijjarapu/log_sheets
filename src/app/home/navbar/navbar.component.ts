import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatAccordion } from '@angular/material/expansion';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { SidenavData } from './../side-navbar';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  showFiller = false;
  username: any;
  panelOpenState = false;
  iconname: any;
  url: any;
  sideMenuItems: any = JSON.parse(JSON.stringify(SidenavData));
  isPanelOpen: any;
  isExpanded: any;
  @ViewChild(MatAccordion) accordion: any;

  @ViewChild('id') id: any;

  constructor(private router: Router) {
    router.events
      .pipe(filter((event: any) => event instanceof NavigationEnd))
      .subscribe((event) => {
        this.url = event['url'];
        console.log(this.url);
      });
  }
  ngOnInit() {
    // const user = JSON.parse(sessionStorage.getItem('carbyne-user'));/
    // this.username = user && user.email;
    // if (this.username) {
    //   const match = this.username.match(/\b(\w)/g);
    // }
    this.iconname = "chevron_right"
    console.log(this.sideMenuItems, "side")
    this.username = localStorage.getItem('currentUser')
    console.log(this.username, "user")
  }
  // onsideNavHover() {
  //   this.showFiller = true;
  //   this.panelOpenState = true;
  //   this.isExpanded = !this.isExpanded;
  // }
  // onsideNavLeave() {
  //   this.showFiller = false;
  //   this.accordion.closeAll();
  // }
  logout() {
    sessionStorage.clear();
    this.router.navigate(['/login']);
  }
  submenuExpansion() {
    this.isExpanded = !this.isExpanded;
    if (this.isExpanded == true) {
      this.iconname = "expand_more"

    }
    else {
      this.iconname = "chevron_right"
    }
  }
}
