import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { MaterialModule } from './material/material.module';
import { HomeModule } from './home/home.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormGroupDirective, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LaunchComponent } from './launch/launch.component';
import { httpInterceptorProviders } from './http-inteceptors';
import { DatePipe } from '@angular/common';
import { AccessdeniedComponent } from './home/accessdenied/accessdenied.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LaunchComponent,
    AccessdeniedComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule, HomeModule,
    FormsModule, ReactiveFormsModule, HttpClientModule
  ],
  providers: [httpInterceptorProviders, DatePipe, FormGroupDirective],
  bootstrap: [AppComponent]
})
export class AppModule { }
