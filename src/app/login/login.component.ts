import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginUser: any;
  loginForm: FormGroup;
  isSubmitted = false;
  hide = true;
  constructor(private fb: FormBuilder, private router: Router) {
    this.loginForm = this.fb.group({
      userID: ['', Validators.required],
      password: ['', Validators.required],
      code: ['', Validators.required]
    });
  }
  // login handler
  login() {
    if (this.loginForm.valid) {
      this.isSubmitted = true;
      console.log("login successfully");
      this.router.navigate(['launch']);
    }
    this.isSubmitted = false;
    const logindetails = this.loginForm.value;
    localStorage.setItem('currentUser', logindetails.userID);
  }
  ngOnInit(): void {
  }

}
